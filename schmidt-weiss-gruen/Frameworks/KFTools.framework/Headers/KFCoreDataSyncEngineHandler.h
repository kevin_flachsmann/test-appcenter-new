//
//  KFCoreDataSyncEngineHandler.h
//  KFTools
//
//  Created by Kevin Flachsmann on 19.05.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <KFTools/KFTools.h>

@interface KFCoreDataSyncEngineHandler : KFSyncEngineHandler
@property (nonatomic) NSManagedObjectContext *managedObjectContext;

- (instancetype)initWithIdentifier:(NSString *)identifier managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (instancetype)initWithIdentifier:(NSString *)identifier timeInterval:(NSTimeInterval)timeInterval backgroundSyncEnabled:(BOOL)backgroundSyncEnabled managedObjectContext:(NSManagedObjectContext *)managedObjectContext NS_DESIGNATED_INITIALIZER;

@end
