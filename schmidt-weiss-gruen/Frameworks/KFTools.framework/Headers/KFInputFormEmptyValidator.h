//
//  KFInputFormEmptyValidator.h
//  Bergzeit
//
//  Created by Kevin Flachsmann on 31.01.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KFInputFormValidatorProtocol.h"

@interface KFInputFormEmptyValidator : NSObject <KFInputFormValidatorProtocol>

@end
