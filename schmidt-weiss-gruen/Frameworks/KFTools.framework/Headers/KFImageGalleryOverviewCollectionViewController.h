//
//  KFImageGalleryOverviewCollectionViewController.h
//  KFTools
//
//  Created by Kevin Flachsmann on 08.11.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>
@class KFImageContainer;

NS_ASSUME_NONNULL_BEGIN

@interface KFImageGalleryOverviewCollectionViewController : UICollectionViewController

- (instancetype)initWithImages:(NSArray<KFImageContainer *> *)images;

@end

NS_ASSUME_NONNULL_END
