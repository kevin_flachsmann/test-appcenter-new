//
//  KFImageViewTableViewCell.h
//  Werbewind
//
//  Created by Kevin Flachsmann on 13.01.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KFImageViewTableViewCell : UITableViewCell
@property (readonly) UIImageView *contentImageView;
- (void)setAspectRatio:(CGFloat)aspectRatio;
@end
