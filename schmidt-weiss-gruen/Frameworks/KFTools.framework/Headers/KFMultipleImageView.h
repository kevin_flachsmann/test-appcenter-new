//
//  KFMultipleImageView.h
//  winter-pro
//
//  Created by Kevin Flachsmann on 07.07.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>


@class KFImageContainer;
@class KFMultipleImageView;

@protocol KFMultipleImageViewDelegate <NSObject>
- (void)multipleImageView:(KFMultipleImageView *)multipleImageView didSelectImageAtIndex:(NSUInteger)index;
@end

@interface KFMultipleImageView : UIView

@property (nonatomic, weak) id<KFMultipleImageViewDelegate> delegate;
@property (nonatomic) NSArray <KFImageContainer *> *images;

- (instancetype)initWithFrame:(CGRect)frame images:(NSArray <KFImageContainer *> *)images;

@end
