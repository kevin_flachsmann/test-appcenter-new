//
//  KFInputFormProtocol.h
//  Bergzeit
//
//  Created by Kevin Flachsmann on 31.01.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#ifndef KFInputFormProtocol_h
#define KFInputFormProtocol_h

#import "KFInputFormDelegate.h"
#import "KFInputFormValidatorProtocol.h"

@protocol KFInputFormProtocol <NSObject>

@property NSString *selector;
@property (nonatomic, weak) id<KFInputFormDelegate> delegate;
@property (nonatomic) id<KFInputFormValidatorProtocol> validator;

- (void)sendValueWithCurrentSelector:(id)value;
- (void)sendValidationWithCurrentSelector:(BOOL)valid;
- (BOOL)isValid:(id)value;

@end

#endif /* KFInputFormProtocol_h */
