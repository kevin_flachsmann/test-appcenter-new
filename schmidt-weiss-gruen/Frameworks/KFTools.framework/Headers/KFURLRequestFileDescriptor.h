//
//  KFURLRequestFileDescriptor.h
//  KFTools
//
//  Created by Kevin Flachsmann on 09.01.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KFURLRequestFileDescriptor : NSObject
@property NSString *fileName;
@property NSString *contentType;
@property NSString *name;
@property NSData *data;

+ (instancetype)descriptorWithData:(NSData *)data name:(NSString *)name fileName:(NSString *)fileName contentType:(NSString *)contentType;

@end
