//
//  KFKeyboardManager.h
//  Werbewind
//
//  Created by Kevin Flachsmann on 09.05.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KFKeyboardManager : NSObject

@property UIView *contentView;
- (instancetype)initWithContentView:(UIView *)contentView;

- (void)startObserving;
- (void)endObserving;
@end
