//
//  KFWeatherEntry.h
//  winter-pro
//
//  Created by Kevin Flachsmann on 29.05.17.
//  Copyright © 2017 Kevin Flachsmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface KFWeatherEntry : NSObject

@property (nonatomic) NSDate *date;
@property (nonatomic) NSString *ISO8601Date;
@property CGFloat temperature;
@property CGFloat relativeHumidity;
@property CGFloat atmosphericPressure;
@property CGFloat windSpeed; //wind_speed_10m:kmh
@property (nonatomic) CGFloat windDirection; //wind_dir_10m:d
@property (readonly) NSString *windDirectionDescription;
@property CGFloat freshSnow; //fresh_snow_1h:cm
@property (nonatomic) NSInteger weatherSymbolCode;
@property (readonly) NSString *weatherSymbolURL;

@end
