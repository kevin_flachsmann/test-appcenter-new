//
//  KFNetworkActivityIndicatorManager.h
//  KFTools
//
//  Created by Kevin Flachsmann on 16.10.15.
//  Copyright © 2015 Kevin Flachsmann. All rights reserved.
//

@import Foundation;

NS_ASSUME_NONNULL_BEGIN

@interface KFNetworkActivityIndicatorManager : NSObject

#pragma mark -
#pragma mark - Singleton Methods

+ (id)sharedInstance;

#pragma mark -
#pragma mark - Activity Count

- (void)decrementActivityCount;
- (void)incrementActivityCount;

@end

NS_ASSUME_NONNULL_END
