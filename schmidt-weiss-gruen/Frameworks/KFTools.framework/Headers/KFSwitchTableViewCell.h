//
//  KFSwitchTableViewCell.h
//  Auf Polizei
//
//  Created by Kevin Flachsmann on 29.01.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KFInputForms.h"

@interface KFSwitchTableViewCell : KFInputFormBaseTableViewCell
@property (readonly) UISwitch *contentSwitch;
@property (readonly) UILabel *contentLabel;
@end
