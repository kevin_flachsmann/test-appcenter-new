//
//  KFImageManager.h
//  KFTools
//
//  Created by Kevin Flachsmann on 17.03.15.
//  Copyright (c) 2015 Kevin Flachsmann. All rights reserved.
//

@import UIKit;
@import Foundation;
@import MapKit;

#import "KFImageManagerCompletion.h"

NS_ASSUME_NONNULL_BEGIN


@interface KFImageManager : NSObject

#pragma mark -
#pragma mark - Singleton Methods

+ (id)sharedInstance;


#pragma mark -
#pragma mark - loading API

- (void)imageForURL:(nullable NSString *)url completion:(nullable KFImageManagerCompletionBlock)completion;
- (void)prefetchImageForURL:(nullable NSString *)url;
- (void)deleteImageForURL:(nullable NSString *)url;

#pragma mark -
#pragma mark - map API

- (void)mapSnapshotForOptions:(nullable MKMapSnapshotOptions *)options annotationImage:(nullable UIImage *)annotationImage completion:(nullable KFImageManagerCompletionBlock)completion;
- (void)prefetchMapSnapshotForOptions:(nullable MKMapSnapshotOptions *)options annotationImage:(nullable UIImage *)annotationImage;


#pragma mark -
#pragma mark - local API

- (void)imageForKey:(nullable NSString *)key completion:(nullable KFImageManagerCompletionBlock)completion;
- (void)putImage:(nullable UIImage *)image forKey:(nullable NSString *)key;
- (void)removeImageForKey:(nullable NSString *)key;


- (void)setNeedsUpdate;

@end

NS_ASSUME_NONNULL_END
