//
//  KFTextViewTableViewCell.h
//  Werbewind
//
//  Created by Kevin Flachsmann on 12.01.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KFInputFormBaseTableViewCell.h"

@interface KFTextViewTableViewCell : KFInputFormBaseTableViewCell
@property (readonly) UITextView *textView;
@end
