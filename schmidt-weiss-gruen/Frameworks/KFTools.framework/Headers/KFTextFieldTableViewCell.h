//
//  KFTextFieldTableViewCell.h
//  Werbewind
//
//  Created by Kevin Flachsmann on 11.01.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KFInputForms.h"

@interface KFTextFieldTableViewCell : KFInputFormBaseTableViewCell
@property (readonly) UITextField *textField;
@end
