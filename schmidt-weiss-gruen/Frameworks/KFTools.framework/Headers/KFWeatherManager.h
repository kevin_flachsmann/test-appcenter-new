//
//  KFWeatherManager.h
//  KFTools
//
//  Created by Kevin Flachsmann on 24.06.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLLocation, KFWeatherEntry;

typedef void(^KFWeatherManagerCompletionBlock)(NSArray <KFWeatherEntry *>* data);

@interface KFWeatherManager : NSObject

- (void)weatherForecastForLocation:(CLLocation *)location startDate:(NSDate *)startDate endDate:(NSDate *)endDate range:(NSInteger)range completion:(KFWeatherManagerCompletionBlock)completion;


- (void)weatherForecastForLocationString:(NSString *)location startDate:(NSDate *)startDate endDate:(NSDate *)endDate range:(NSInteger)range completion:(KFWeatherManagerCompletionBlock)completion;

@end
