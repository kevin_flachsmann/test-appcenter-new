//
//  UIView+KFTools.h
//  KFTools
//
//  Created by Kevin Flachsmann on 16.02.17.
//  Copyright © 2017 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>
@class KFBadgeView;

NS_ASSUME_NONNULL_BEGIN

@interface UIView (KFTools)

- (nullable UIViewController *)parentViewController;
- (nullable UIView *)getFirstResponder;

@property (nonatomic, readonly) KFBadgeView *badge;

@end

NS_ASSUME_NONNULL_END
