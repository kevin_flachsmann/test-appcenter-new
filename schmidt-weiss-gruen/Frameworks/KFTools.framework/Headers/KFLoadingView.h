//
//  KFLoadingView.h
//  KFTools
//
//  Created by Kevin Flachsmann on 05.03.15.
//  Copyright (c) 2015 Kevin Flachsmann. All rights reserved.
//

/*
 *  VERSION 1.3
 */

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@class KFLoadingView;

@protocol KFLoadingViewDelegate <NSObject>
- (void)loadingViewReloadPressed:(KFLoadingView *)loadingView;
@end

@interface KFLoadingView : UIView
@property (nullable, nonatomic, weak) id<KFLoadingViewDelegate> delegate;

- (void)hide;
- (void)show;
- (void)showInView:(UIView *)view;
- (void)showInViewController:(UIViewController *)viewController;

- (void)showLoadingError;

- (void)setAnimationColor:(UIColor *)color;

@end

NS_ASSUME_NONNULL_END
