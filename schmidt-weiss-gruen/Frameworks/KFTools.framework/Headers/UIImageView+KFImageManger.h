//
//  UIImageView+KFImageManger.h
//  KFTools
//
//  Created by Kevin Flachsmann on 17.02.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

@import UIKit;
@import MapKit;

@class KFImageContainer;

NS_ASSUME_NONNULL_BEGIN

@interface UIImageView (KFImageManger)

- (void)setImageContainer:(KFImageContainer *)image;

- (void)setImageWithURL:(nullable NSString *)url placeholderImage:(nullable UIImage *)placeholder;
- (void)setImageWithYoutubeURL:(nullable NSString *)url placeholderImage:(nullable UIImage *)placeholder __deprecated;
- (void)setImageWithYoutubeID:(nullable NSString *)youtubeID placeholderImage:(nullable UIImage *)placeholder __deprecated;


- (void)setMapSnapshotWithCoordinate:(CLLocationCoordinate2D)coordinate annotationImage:(nullable UIImage *)annotationImage placeholderImage:(nullable UIImage *)placeholder;
- (void)setMapSnapshotWithCoordinate:(CLLocationCoordinate2D)coordinate mapType:(MKMapType)mapType annotationImage:(nullable UIImage *)annotationImage placeholderImage:(nullable UIImage *)placeholder;

- (void)setImageWithKey:(nullable NSString *)key placeholderImage:(nullable UIImage *)placeholder;


- (void)addTarget:(id)target action:(SEL)action;

- (void)setHasBorder:(BOOL)border;

@end

NS_ASSUME_NONNULL_END
