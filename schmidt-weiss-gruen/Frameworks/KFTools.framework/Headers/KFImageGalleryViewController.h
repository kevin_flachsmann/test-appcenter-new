//
//  KFImageGalleryViewController.h
//  KFTools
//
//  Created by Kevin Flachsmann on 10.10.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>
@class KFImageContainer;

NS_ASSUME_NONNULL_BEGIN

typedef  UIView * _Nullable (^KFImageGalleryDismissBlock)(NSInteger index);

@interface KFImageGalleryViewController : UIViewController

- (instancetype)initWithImages:(NSArray<KFImageContainer *> *)images position:(NSInteger)position referenceView:(nullable UIView *)referenceView;

@property (nullable) KFImageGalleryDismissBlock dismissBlock;

@end

NS_ASSUME_NONNULL_END
