//
//  KFSyncEngineHandler.h
//  Bergzeit
//
//  Created by Kevin Flachsmann on 05.02.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^KFSyncEngineHandlerCompletionBlock)(NSString *identifier, BOOL success);

@interface KFSyncEngineHandler : NSObject

//internal use only
@property KFSyncEngineHandlerCompletionBlock completion;
@property BOOL isBackgroundSync;
- (void)finishSync:(BOOL)success;


- (instancetype)initWithIdentifier:(NSString *)identifier;
- (instancetype)initWithIdentifier:(NSString *)identifier timeInterval:(NSTimeInterval)timeInterval backgroundSyncEnabled:(BOOL)backgroundSyncEnabled NS_DESIGNATED_INITIALIZER;

@property NSTimeInterval timeInterval;
@property BOOL backgroundSyncEnabled;
@property NSString *identifier;

- (void)startSync;

@end

NS_ASSUME_NONNULL_END
