//
//  KFInputFormDelegate.h
//  Werbewind
//
//  Created by Kevin Flachsmann on 09.01.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#ifndef KFInputFormDelegate_h
#define KFInputFormDelegate_h

@protocol KFInputFormDelegate <NSObject>

@optional
- (void)didSetValue:(id)value forSelector:(NSString *)selector;
- (void)didValidate:(BOOL)valid forSelector:(NSString *)selector;

@end


#endif /* KFInputFormDelegate_h */
