//
//  KFCardCollectionViewCell.h
//  Maschinenring
//
//  Created by Kevin Flachsmann on 25.02.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KFCardCollectionViewCell : UICollectionViewCell
@property (readonly) UIView *cardView;
@property (nonatomic) UIEdgeInsets cardInsets;
@property (nonatomic) UIColor *selectedColor;
@property (nonatomic) UIColor *defaultColor;
@end
