//
//  KFRichTextView.h
//  KFTools
//
//  Created by Kevin Flachsmann on 23.01.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class KFRichTextView;

@protocol KFRichTextViewDelegate <NSObject>

@required
- (void)richTextViewDidBecomeReady:(KFRichTextView *)textView;

@optional
- (void)richTextViewDidChange:(KFRichTextView *)textView;
- (void)richTextViewDidBeginEditing:(KFRichTextView *)textView;
- (void)richTextViewDidEndEditing:(KFRichTextView *)textView;

@end

@interface KFRichTextView : UIView
@property (nullable, nonatomic, weak) id<KFRichTextViewDelegate> delegate;
@property (nullable) NSString *htmlString;

- (void)setBold;
- (void)setItalic;
- (void)setDefaultTextColor:(UIColor *)color;
- (void)insertLinkWithURL:(NSString *)url andTitle:(NSString *)title;

- (void)setInputAccessoryView:(UIView *)view;

@end

NS_ASSUME_NONNULL_END
