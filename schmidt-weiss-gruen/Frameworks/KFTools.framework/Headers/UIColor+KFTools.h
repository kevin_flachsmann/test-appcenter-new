//
//  UIColor+KFTools.h
//  Auf Polizei
//
//  Created by Kevin Flachsmann on 27.09.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

@import UIKit;

@interface UIColor (KFTools)

- (NSString *)hexString;
+ (UIColor *)colorWithHexString:(NSString *)string;
+ (UIColor *)colorRGBWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha;

@end
