//
//  UIViewController+KFTabBarController.h
//  Bergzeit
//
//  Created by Kevin Flachsmann on 01.02.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (KFTabBarController)
@property (nonatomic, strong) UIButton *KFTabBarButton;
@end
