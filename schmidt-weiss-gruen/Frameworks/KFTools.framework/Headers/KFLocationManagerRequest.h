//
//  KFLocationManagerRequest.h
//  winter-pro
//
//  Created by Kevin Flachsmann on 25.05.17.
//  Copyright © 2017 Kevin Flachsmann. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CLLocation;

typedef enum {
    KFLocationManagerRequestTypeNone,
    KFLocationManagerRequestTypeSingle,
    KFLocationManagerRequestTypeSubscription,
    KFLocationManagerRequestTypeBackgroundSubscription
} KFLocationManagerRequestType;

typedef enum {
    KFLocationManagerRequestAccuracyDefault,
    KFLocationManagerRequestAccuracyMedium,
    KFLocationManagerRequestAccuracyBest
} KFLocationManagerRequestAccuracy;

typedef enum {
    KFLocationManagerRequestDistanceFilterDefault,
    KFLocationManagerRequestDistanceFilterHigh,
    KFLocationManagerRequestDistanceFilterSmall
} KFLocationManagerRequestDistanceFilter;

typedef void(^KFLocationManagerRequestCompletionBlock)(CLLocation *location);

@interface KFLocationManagerRequest : NSObject

@property KFLocationManagerRequestType requestType;
@property KFLocationManagerRequestAccuracy requestAccuracy;
@property KFLocationManagerRequestDistanceFilter distanceFilter;
@property KFLocationManagerRequestCompletionBlock completion;


- (instancetype)initWithRequestType:(KFLocationManagerRequestType)requestType requestAccuracy:(KFLocationManagerRequestAccuracy)requestAccuracy completion:(KFLocationManagerRequestCompletionBlock)completion;

- (instancetype)initWithRequestType:(KFLocationManagerRequestType)requestType requestAccuracy:(KFLocationManagerRequestAccuracy)requestAccuracy distanceFilter:(KFLocationManagerRequestDistanceFilter)distanceFilter completion:(KFLocationManagerRequestCompletionBlock)completion;

- (BOOL)updateWithLocations:(NSArray *)locations;

@end
