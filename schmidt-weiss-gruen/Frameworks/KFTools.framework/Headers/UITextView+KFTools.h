//
//  UITextView+KFTools.h
//  Auf Polizei
//
//  Created by Kevin Flachsmann on 06.06.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

@import UIKit;


@interface UITextView (KFTools)

@property (nonatomic, readonly) UILabel *placeholderLabel;

@property (nonatomic, strong) IBInspectable NSString *placeholder;
@property (nonatomic, strong) NSAttributedString *attributedPlaceholder;
@property (nonatomic, strong) IBInspectable UIColor *placeholderColor;

+ (UIColor *)defaultPlaceholderColor;

- (void)addDoneButton;

@end
