//
//  KFWebViewController.h
//  KFTools
//
//  Created by Kevin Flachsmann on 31.10.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

#ifndef KFTOOLS_EXTENSION

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KFWebViewController : UIViewController

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithNibName:(nullable NSString *)nibNameOrNil bundle:(nullable NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;

//file initialisation
- (instancetype)initWithContentFile:(nullable NSString *)contentFile;

//string initialisation
- (instancetype)initWithContentString:(nullable NSString *)contentString;


@end

NS_ASSUME_NONNULL_END

#endif
