//
//  KFLocationManagerBeaconRequest.h
//  winter-pro
//
//  Created by Kevin Flachsmann on 21.06.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLBeaconRegion, CLBeacon;

typedef void(^KFLocationManagerBeaconRequestCompletionBlock)(NSArray<CLBeacon *> *beacons);

@interface KFLocationManagerBeaconRequest : NSObject

@property (readonly) CLBeaconRegion *region;

@property KFLocationManagerBeaconRequestCompletionBlock completion;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithRegion:(CLBeaconRegion *)region completion:(KFLocationManagerBeaconRequestCompletionBlock)completion;

- (void)updateWithBeacons:(NSArray<CLBeacon *> *)beacons;

@end
