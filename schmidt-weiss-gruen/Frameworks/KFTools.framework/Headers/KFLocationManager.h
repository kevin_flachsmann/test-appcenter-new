//
//  KFLocationManager.h
//  winter-pro
//
//  Created by Kevin Flachsmann on 25.05.17.
//  Copyright © 2017 Kevin Flachsmann. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KFLocationManagerRequest, KFLocationManagerBeaconRequest;

@interface KFLocationManager : NSObject

+ (id)sharedInstance;

- (void)addRequest:(KFLocationManagerRequest *)request;
- (void)addRequests:(NSArray *)requests;
- (void)removeRequest:(KFLocationManagerRequest *)request;
- (void)removeRequests:(NSArray *)requests;

- (void)addBeaconRequest:(KFLocationManagerBeaconRequest *)request;
- (void)removeBeaconRequest:(KFLocationManagerBeaconRequest *)request;
- (void)removeBeaconRequests:(NSArray *)requests;


@property NSString *errorMessageTitle;
@property NSString *errorMessageDescription;

//- (CLLocation *)currentLocation;
//- (NSString *)currentLocationName;

@end
