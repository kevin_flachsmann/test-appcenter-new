//
//  KFCardTableViewCell.h
//  Bergzeit
//
//  Created by Kevin Flachsmann on 05.02.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KFCardView.h"

@interface KFCardTableViewCell : UITableViewCell

@property (readonly) KFCardView *cardView;
@property (nonatomic) UIEdgeInsets cardInsets;

@property (nonatomic) UIColor *selectedColor;
@property (nonatomic) UIColor *defaultColor;

@end
