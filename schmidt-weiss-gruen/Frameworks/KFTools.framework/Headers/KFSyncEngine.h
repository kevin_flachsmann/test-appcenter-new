//
//  KFSyncEngine.h
//  Bergzeit
//
//  Created by Kevin Flachsmann on 05.02.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KFSyncEngineHandler.h"

NS_ASSUME_NONNULL_BEGIN

extern NSString *const KFSyncEngineSyncDidFinishNotification;
typedef void(^UIBackgroundFetchResultCompletionHandler)(UIBackgroundFetchResult result);

@interface KFSyncEngine : NSObject

+ (id)sharedInstance;
- (void)registerSyncHandler:(KFSyncEngineHandler *)handler;
- (void)startSync;
- (void)startSyncForIdentifier:(NSString *)identifier;
- (void)startBackgroundSync:(UIBackgroundFetchResultCompletionHandler)completion;

@end

NS_ASSUME_NONNULL_END
