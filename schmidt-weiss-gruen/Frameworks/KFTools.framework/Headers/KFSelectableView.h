//
//  KFSelectableView.h
//  KFTools
//
//  Created by Kevin Flachsmann on 19.06.15.
//  Copyright (c) 2015 Kevin Flachsmann. All rights reserved.
//

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

IB_DESIGNABLE

@interface KFSelectableView : UIControl

@property (nonatomic) IBInspectable UIColor *selectedColor;

@end

NS_ASSUME_NONNULL_END
