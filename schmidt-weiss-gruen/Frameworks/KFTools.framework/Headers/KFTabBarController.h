//
//  KFTabBarController.h
//  Bergzeit
//
//  Created by Kevin Flachsmann on 01.02.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+KFTabBarController.h"

extern NSString *const KFTabBarControllerViewControllerChangedNotification;
extern NSString *const KFTabBarControllerViewControllerAlreadyVisibleNotification;

@class KFTabBarController;

@protocol KFTabBarControllerDelegate <NSObject>

@optional
- (void)tabBarController:(KFTabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController atIndex:(NSUInteger)index;
@end

@interface KFTabBarController : UIViewController

- (instancetype)initWithViewController:(NSArray <__kindof UIViewController *> *)viewControllers;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE;

@property (nonatomic) NSArray <__kindof UIViewController *> *viewControllers;
@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic, weak) id<KFTabBarControllerDelegate> delegate;

- (void)setTabBarColor:(UIColor *)color;
- (void)setHairlineColor:(UIColor *)color;

@end
