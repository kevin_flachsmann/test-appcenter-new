//
//  UIAlertController+KFTools.h
//  KFTools
//
//  Created by Kevin Flachsmann on 18.05.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@interface UIAlertController (KFTools)

- (void)show;
- (void)show:(BOOL)animated;

- (void)addActionWithTitle:(nullable NSString *)title style:(UIAlertActionStyle)style handler:(void (^ __nullable)(UIAlertAction *action))handler;

@end

NS_ASSUME_NONNULL_END
