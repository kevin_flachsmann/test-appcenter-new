//
//  KFInputFormValidatorProtocol.h
//  Bergzeit
//
//  Created by Kevin Flachsmann on 31.01.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#ifndef KFInputFormValidatorProtocol_h
#define KFInputFormValidatorProtocol_h

@protocol KFInputFormValidatorProtocol <NSObject>
- (BOOL)isValid:(id)value;
@end

#endif /* KFInputFormValidatorProtocol_h */
