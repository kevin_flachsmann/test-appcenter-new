//
//  KFCardView.h
//  Maschinenring
//
//  Created by Kevin Flachsmann on 25.02.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KFCardView : UIView
@property UIEdgeInsets shadowInsets;
@property (nonatomic) BOOL drawsShadow;
@end
