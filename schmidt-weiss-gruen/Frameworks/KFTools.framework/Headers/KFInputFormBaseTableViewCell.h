//
//  KFInputFormTableViewCell.h
//  Werbewind
//
//  Created by Kevin Flachsmann on 31.01.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KFInputFormProtocol.h"

@interface KFInputFormBaseTableViewCell : UITableViewCell <KFInputFormProtocol>

@end
