//
//  KFImageManagerCompletion.h
//  KFTools
//
//  Created by Kevin Flachsmann on 21.08.17.
//  Copyright © 2017 Kevin Flachsmann. All rights reserved.
//

#ifndef KFImageManagerCompletion_h
#define KFImageManagerCompletion_h

@class UIImage;

NS_ASSUME_NONNULL_BEGIN

typedef void(^KFImageManagerCompletionBlock)(UIImage * _Nullable image);

NS_ASSUME_NONNULL_END

#endif /* KFImageManagerCompletion_h */
