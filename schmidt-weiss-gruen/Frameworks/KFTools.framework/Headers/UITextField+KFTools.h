//
//  UITextField+KFTools.h
//  KFTools
//
//  Created by Kevin Flachsmann on 21.12.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (KFTools)

- (void)addDoneButton;

@end
