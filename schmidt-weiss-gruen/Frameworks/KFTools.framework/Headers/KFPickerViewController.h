//
//  KFPickerViewController.h
//  KFTools
//
//  Created by Kevin Flachsmann on 04.10.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

#ifndef KFTOOLS_EXTENSION

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

typedef void (^KFPickerViewControllerDateCompletionBlock)(NSDate *selectedDate);
typedef void (^KFPickerViewControllerStringCompletionBlock)(NSInteger index, NSString *selectedValue);

@interface KFPickerViewController : UIViewController
- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithNibName:(nullable NSString *)nibNameOrNil bundle:(nullable NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;

- (instancetype)initWithDatePickerMode:(UIDatePickerMode)pickerMode date:(nullable NSDate *)date title:(nullable NSString *)title completion:(nullable KFPickerViewControllerDateCompletionBlock)completion;
- (instancetype)initWithArray:(nullable NSArray<NSString *> *)array title:(nullable NSString *)title completion:(nullable KFPickerViewControllerStringCompletionBlock)completion;
- (instancetype)initWithCustomPicker:(nullable UIPickerView *)pickerView title:(nullable NSString *)title completion:(void(^)(void))completion;

@property UIColor *tintColor;
- (void)setPickerPosition:(NSInteger)position;
- (void)setPickerPositionWithValue:(nullable NSString *)value;
- (nullable UIDatePicker *)datePicker;

@end

NS_ASSUME_NONNULL_END

#endif
