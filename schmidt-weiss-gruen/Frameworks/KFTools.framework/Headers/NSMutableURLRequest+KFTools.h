//
//  NSMutableURLRequest+KFTools.h
//  KFTools
//
//  Created by Kevin Flachsmann on 09.01.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KFURLRequestFileDescriptor;

@interface NSMutableURLRequest (KFTools)
+ (instancetype)requestWithURL:(NSString *)urlString andParameters:(NSDictionary <NSString *, NSString *>*)parameters;
- (void)setHTTPBodyWithParameters:(NSDictionary <NSString *, NSString *>*)parameters;
- (void)setHTTPBodyWithParameters:(NSDictionary <NSString *, NSString *>*)parameters files:(NSArray <KFURLRequestFileDescriptor *> *)files;
@end
