//
//  UIImage+KFTools.h
//  KFTools
//
//  Created by Kevin Flachsmann on 15.11.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (KFTools)

+ (UIImage *)splashImage;
+ (UIImage *)imageNamed:(NSString *)name tintColor:(UIColor *)color;

@end
