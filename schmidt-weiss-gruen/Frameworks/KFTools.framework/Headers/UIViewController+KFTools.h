//
//  UIViewController+KFTools.h
//  KFTools
//
//  Created by Kevin Flachsmann on 08.11.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (KFTools)

- (BOOL)isPresented;
@property (nullable) NSString *navigationItemLogoName;

@end

NS_ASSUME_NONNULL_END
