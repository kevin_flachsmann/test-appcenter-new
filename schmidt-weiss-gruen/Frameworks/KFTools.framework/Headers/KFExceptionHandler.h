//
//  KFExceptionHandler.h
//  KFTools
//
//  Created by Kevin Flachsmann on 25.04.17.
//  Copyright © 2017 Kevin Flachsmann. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KFExceptionHandler : NSObject

+ (void)initWithName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
