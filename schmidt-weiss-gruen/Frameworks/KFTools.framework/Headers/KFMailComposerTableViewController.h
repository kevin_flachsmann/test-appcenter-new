//
//  KFMailComposerTableViewController.h
//  KFTools
//
//  Created by Kevin Flachsmann on 08.11.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KFMailComposerTableViewController : UITableViewController

//- (instancetype)init NS_UNAVAILABLE;
//- (instancetype)initWithNibName:(nullable NSString *)nibNameOrNil bundle:(nullable NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
//- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;

@end
