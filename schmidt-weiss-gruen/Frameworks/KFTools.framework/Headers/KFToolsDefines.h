//
//  KFToolsDefines.h
//  KFTools
//
//  Created by Kevin Flachsmann on 14.10.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

#ifndef KFToolsDefines_h
#define KFToolsDefines_h

#ifdef DEBUG
#define DLog(fmt, ...) NSLog((@"%s " fmt), __PRETTY_FUNCTION__, ##__VA_ARGS__)
#else
#define DLog(...)
#endif

#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad


#endif /* KFToolsDefines_h */
