//
//  NSLayoutConstraint+KFLayoutConstraint.h
//  winter-pro
//
//  Created by Kevin Flachsmann on 26.10.15.
//  Copyright © 2015 Kevin Flachsmann. All rights reserved.
//

@import UIKit;

IB_DESIGNABLE

@interface NSLayoutConstraint (KFTools)
@property (nonatomic) IBInspectable NSInteger preciseConstant;
@end
