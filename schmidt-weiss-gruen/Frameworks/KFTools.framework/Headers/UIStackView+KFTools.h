//
//  UIStackView+KFTools.h
//  KFTools
//
//  Created by Kevin Flachsmann on 23.05.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStackView (KFTools)
- (void)setBackgroundColor:(UIColor *)color;
- (void)addSeparatorLine;
@end
