//
//  KFRichTextEditorViewController.h
//  KFTools
//
//  Created by Kevin Flachsmann on 29.01.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KFRichTextView.h"

@interface KFRichTextEditorViewController : UIViewController <KFRichTextViewDelegate>
@property (readonly) KFRichTextView *textView;
@property (readonly) UIToolbar *toolbar;
@end
