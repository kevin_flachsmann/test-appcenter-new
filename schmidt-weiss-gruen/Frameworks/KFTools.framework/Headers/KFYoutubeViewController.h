//
//  KFYoutubeViewController.h
//  KFTools
//
//  Created by Kevin Flachsmann on 13.08.15.
//  Copyright (c) 2015 Kevin Flachsmann. All rights reserved.
//

#ifndef KFTOOLS_EXTENSION

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@interface KFYoutubeViewController : UIViewController

- (instancetype)init NS_UNAVAILABLE;
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithNibName:(nullable NSString *)nibNameOrNil bundle:(nullable NSBundle *)nibBundleOrNil NS_UNAVAILABLE;

- (instancetype)initWithYoutubeID:(NSString *)youtubeID;
@end

NS_ASSUME_NONNULL_END

#endif
