//
//  KFImageGalleryImage.h
//  KFTools
//
//  Created by Kevin Flachsmann on 10.10.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

typedef enum ImageType : NSUInteger {
    ImageTypeDefault,
    ImageTypeKey,
    ImageTypeUrl,
    ImageTypeMap
} ImageType;

@interface KFImageContainer : NSObject

//placeholder
@property (nullable) UIImage *placeholder;

//default image
@property (readonly) UIImage *image;

//local image
@property (readonly) NSString *imageKey;

//remote image
@property (readonly) NSString *imageURL;

//description
@property (nullable) NSString *imageDescription;

//type
@property (readonly) ImageType imageType;



/*
 initialisation
 */

//default image
- (instancetype)initWithImage:(nullable UIImage *)image;

//default image
- (instancetype)initWithImageKey:(nullable NSString *)imageKey;
- (instancetype)initWithImageKey:(nullable NSString *)imageKey placeholder:(nullable UIImage *)placeholder;

//remote image
- (instancetype)initWithImageURL:(nullable NSString *)imageURL;
- (instancetype)initWithImageURL:(nullable NSString *)imageURL placeholder:(nullable UIImage *)placeholder;

//youtube support
- (instancetype)initWithYoutubeURL:(nullable NSString *)url placeholderImage:(nullable UIImage *)placeholder;
- (instancetype)initWithYoutubeID:(nullable NSString *)youtubeID placeholderImage:(nullable UIImage *)placeholder;

@end

NS_ASSUME_NONNULL_END
