//
//  KFImagePagingView.h
//  KFTools
//
//  Created by Kevin Flachsmann on 23.06.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//


@import UIKit;
@class KFImageContainer;

NS_ASSUME_NONNULL_BEGIN

@interface KFImagePagingView : UIView

- (void)setImages:(nullable NSArray<KFImageContainer *> *)images;
- (void)startSlideShowWithTimeInterval:(NSTimeInterval)timeInterval;

@end

NS_ASSUME_NONNULL_END
