//
//  NSDate+KFTools.h
//  Maschinenring Hannover
//
//  Created by Kevin Flachsmann on 15.09.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

@import Foundation;

@interface NSDate (KFTools)

+ (NSString *)rangeStringWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate;
- (NSString *)formattedString;

+ (NSDate*)roundDateUp:(NSDate *)date toNearestMinutes:(NSInteger)minutes;

@end
