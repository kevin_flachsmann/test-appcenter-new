//
//  KFGradientView.h
//  KFTools
//
//  Created by Kevin Flachsmann on 24.06.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

/*
 *  VERSION 1.2
 */

#import <UIKit/UIKit.h>

typedef enum {
    KFGradientViewStyleTopBottom,
    KFGradientViewStyleLeftRight
}KFGradientViewStyle;

NS_ASSUME_NONNULL_BEGIN
IB_DESIGNABLE

@interface KFGradientView : UIView
@property (nonatomic) IBInspectable UIColor *startColor;
@property (nonatomic) IBInspectable UIColor *endColor;
@property (nonatomic) KFGradientViewStyle gradientStyle;

- (instancetype)initWithFrame:(CGRect)frame startColor:(UIColor *)startColor endColor:(UIColor *)endColor style:(KFGradientViewStyle)style;
@end

NS_ASSUME_NONNULL_END
