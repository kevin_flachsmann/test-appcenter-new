//
//  NSDictionary+KFTools.h
//  KFTools
//
//  Created by Kevin Flachsmann on 07.03.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

@import Foundation;

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (KFTools)

- (nullable NSDictionary *)dictionaryByReplacingNullsWithStrings;
- (BOOL)containsKey:(nullable NSString *)key;
- (NSString *)stringForKey:(nullable NSString *)key;
- (NSNumber *)numberForKey:(nullable NSString *)key;
- (NSDate *)dateForKey:(nullable NSString *)key;

@end

NS_ASSUME_NONNULL_END
