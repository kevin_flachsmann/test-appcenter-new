//
//  NSManagedObjectContext+KFTools.h
//  KFTools
//
//  Created by Kevin Flachsmann on 24.05.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (KFTools)

- (void)deleteAllObjectsForEntityName:(NSString *)entityName;
- (void)deleteAllObjectsForEntityName:(NSString *)entityName predicate:(NSPredicate *)predicate;

//single object
- (NSManagedObject *)objectForEntityName:(NSString *)entityName;
- (NSManagedObject *)objectForEntityName:(NSString *)entityName predicate:(NSPredicate *)predicate;

//multiple objects
- (NSArray *)allObjectsForEntityName:(NSString *)entityName;
- (NSArray *)allObjectsForEntityName:(NSString *)entityName predicate:(NSPredicate *)predicate;
- (NSArray *)allObjectsForEntityName:(NSString *)entityName predicate:(NSPredicate *)predicate sortDescriptors:(NSArray<NSSortDescriptor *> *)sortDescriptors;

//count
- (NSUInteger)countForEntityName:(NSString *)entityName;
- (NSUInteger)countForEntityName:(NSString *)entityName predicate:(NSPredicate *)predicate;

//max

@end
