//
//  KFMapViewController.h
//  KFTools
//
//  Created by Kevin Flachsmann on 12.09.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

@import UIKit;
@class KFAnnotation;

NS_ASSUME_NONNULL_BEGIN

@interface KFMapViewController : UIViewController

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithNibName:(nullable NSString *)nibNameOrNil bundle:(nullable NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;

- (instancetype)initWithAnnotations:(NSArray<KFAnnotation *> *)annotations;
- (void)setTintColorForSegmentedControl:(UIColor *)tintColor;

@end

NS_ASSUME_NONNULL_END
