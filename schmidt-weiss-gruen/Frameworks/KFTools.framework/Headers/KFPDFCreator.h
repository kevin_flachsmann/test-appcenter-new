//
//  KFPDFCreator.h
//  KFTools
//
//  Created by Kevin Flachsmann on 05.10.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

#ifndef KFTOOLS_EXTENSION

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN 

typedef void (^KFPDFCreatorCompletionBlock)(BOOL success);

@interface KFPDFCreator : NSObject
+ (void)createPFDWithHTML:(NSString *)html filePath:(NSString *)filePath baseURL:(NSURL *)baseURL completion:(KFPDFCreatorCompletionBlock)completion;
- (void)createPFDWithHTML:(NSString *)html filePath:(NSString *)filePath baseURL:(NSURL *)baseURL completion:(KFPDFCreatorCompletionBlock)completion;
@end

NS_ASSUME_NONNULL_END


#endif
