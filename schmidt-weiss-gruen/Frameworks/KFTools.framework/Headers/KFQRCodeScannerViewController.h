//
//  KFQRCodeScannerViewController.h
//  KFTools
//
//  Created by Kevin Flachsmann on 11.01.17.
//  Copyright © 2017 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^KFQRCodeScannerViewControllerCompletionBlock)(NSString *scannedValue);

@interface KFQRCodeScannerViewController : UIViewController

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithNibName:(nullable NSString *)nibNameOrNil bundle:(nullable NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;

- (instancetype)initWithCompletion:(nullable KFQRCodeScannerViewControllerCompletionBlock)completion;

@end

NS_ASSUME_NONNULL_END
