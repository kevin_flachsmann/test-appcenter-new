//
//  KFTools.h
//  KFTools
//
//  Created by Kevin Flachsmann on 13.10.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

@import UIKit;

//! Project version number for KFTools.
FOUNDATION_EXPORT double KFToolsVersionNumber;

//! Project version string for KFTools.
FOUNDATION_EXPORT const unsigned char KFToolsVersionString[];


//
//  Defines
//
#import <KFTools/KFToolsDefines.h>

//
//  Database
//
#import <KFTools/KFDataManager.h>


//
//  Extensions
//
#import <KFTools/NSDate+KFTools.h>
#import <KFTools/NSDictionary+KFTools.h>
#import <KFTools/NSString+KFTools.h>
#import <KFTools/NSLayoutConstraint+KFTools.h>
#import <KFTools/UIAlertController+KFTools.h>
#import <KFTools/UIColor+KFTools.h>
#import <KFTools/UITextView+KFTools.h>
#import <KFTools/UITextField+KFTools.h>
#import <KFTools/UIViewController+KFTools.h>
#import <KFTools/UIImage+KFTools.h>
#import <KFTools/UIView+KFTools.h>
#import <KFTools/NSMutableURLRequest+KFTools.h>
#import <KFTools/UIStackView+KFTools.h>
#import <KFTools/NSManagedObjectContext+KFTools.h>


//
//  UI
//

//input forms
#import <KFTools/KFInputForms.h>

//cells
#import <KFTools/KFButtonTableViewCell.h>
#import <KFTools/KFCardCollectionViewCell.h>
#import <KFTools/KFCardTableViewCell.h>
#import <KFTools/KFImageViewTableViewCell.h>
#import <KFTools/KFTextFieldTableViewCell.h>
#import <KFTools/KFTextViewTableViewCell.h>
#import <KFTools/KFSwitchTableViewCell.h>
#import <KFTools/KFCardView.h>

//tabbar
#import <KFTools/KFTabBarController.h>
#import <KFTools/UIViewController+KFTabBarController.h>

#import <KFTools/KFMultipleImageView.h>

#import <KFTools/KFMapViewController.h>
#import <KFTools/KFAnnotation.h>

#import <KFTools/KFLoadingView.h>
#import <KFTools/KFSelectableView.h>
#import <KFTools/KFGradientView.h>
#import <KFTools/KFBadgeView.h>
#import <KFTools/KILabel.h>

#import <KFTools/KFImageContainer.h>
#import <KFTools/KFImageGalleryViewController.h>
#import <KFTools/KFImageGalleryOverviewCollectionViewController.h>
#import <KFTools/KFImagePagingView.h>

#import <KFTools/KFYoutubeViewController.h>

#import <KFTools/KFPopUpViewController.h>

#import <KFTools/PickerCells.h>
#import <KFTools/KFPickerViewController.h>

#import <KFTools/KFWebViewController.h>

#import <KFTools/KFMailComposerTableViewController.h>
#import <KFTools/KFQRCodeScannerViewController.h>

#import <KFTools/KFRichTextView.h>
#import <KFTools/KFRichTextEditorViewController.h>

#import <KFTools/KFKeyboardManager.h>

//
//  Network
//
#import <KFTools/KFImageManager.h>
#import <KFTools/UIImageView+KFImageManger.h>
#import <KFTools/KFImageContainer.h>

#import <KFTools/KFNetworkActivityIndicatorManager.h>
#import <KFTools/KFConnectionManager.h>
#import <KFTools/KFURLRequestFileDescriptor.h>
#import <KFTools/Reachability.h>



//
//  Other
//
#import <KFTools/KFPDFCreator.h>
#import <KFTools/KFExceptionHandler.h>
#import <KFTools/KFSyncEngine.h>
#import <KFTools/KFSyncEngineHandler.h>
#import <KFTools/KFCoreDataSyncEngineHandler.h>
#import <KFTools/KFWeatherManager.h>
#import <KFTools/KFWeatherEntry.h>

//loaction

#import <KFTools/KFLocationManager.h>
#import <KFTools/KFLocationManagerRequest.h>
#import <KFTools/KFLocationManagerBeaconRequest.h>

