//
//  KFAnnotation.h
//  KFTools
//
//  Created by Kevin Flachsmann on 21.08.15.
//  Copyright (c) 2015 Kevin Flachsmann. All rights reserved.
//

@import MapKit;

NS_ASSUME_NONNULL_BEGIN

@interface KFAnnotation : NSObject <MKAnnotation>

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly, copy, nullable) NSString *title;
@property (nonatomic, readonly, copy, nullable) NSString *subtitle;
@property (nonatomic, copy, nullable) NSString *reuseIdentifier;
@property (nonatomic, readonly, nullable) UIImage *image;

@property UIColor *pinColor;
@property NSInteger tag;

- (instancetype)init NS_UNAVAILABLE; 
- (instancetype)initWithTitle:(nullable NSString *)title subtitle:(nullable NSString *)subtitle latitude:(double)latitude longitude:(double)longitude NS_DESIGNATED_INITIALIZER;
- (instancetype)initWithTitle:(nullable NSString *)title subtitle:(nullable NSString *)subtitle pinColor:(nullable UIColor *)pinColor latitude:(double)latitude longitude:(double)longitude NS_DESIGNATED_INITIALIZER;
- (instancetype)initWithTitle:(nullable NSString *)title subtitle:(nullable NSString *)subtitle image:(nullable UIImage *)image latitude:(double)latitude longitude:(double)longitude NS_DESIGNATED_INITIALIZER;

- (MKAnnotationView *)annotationView;

@end

NS_ASSUME_NONNULL_END
