//
//  KFConnectionManager.h
//  KFTools
//
//  Created by Kevin Flachsmann on 16.02.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

@import Foundation;
@class UIImage;

typedef void(^KFConnectionManagerCompletionBlock)(NSData *receivedData);
typedef void(^KFConnectionManagerJSONCompletionBlock)(NSDictionary *receivedJSON);
typedef void(^KFConnectionManagerFailureBlock)(void);

typedef BOOL(^KFConnectionManagerValidationBlock)(NSDictionary *receivedJSON);

@interface KFConnectionManager : NSObject


#pragma mark -
#pragma mark - Singleton Methods

+ (id)sharedInstance;

#pragma mark -
#pragma mark - Response validation

@property KFConnectionManagerValidationBlock validationBlock;
- (void)setValidationBlock:(KFConnectionManagerValidationBlock)validationBlock;

#pragma mark -
#pragma mark - data requests

- (void)sendRequest:(NSURLRequest *)request completion:(KFConnectionManagerCompletionBlock)completion failure:(KFConnectionManagerFailureBlock)failure;

#pragma mark -
#pragma mark - JSON requests

- (void)sendJSONRequest:(NSURLRequest *)request completion:(KFConnectionManagerJSONCompletionBlock)completion failure:(KFConnectionManagerFailureBlock)failure;
- (void)sendJSONRequest:(NSURLRequest *)request synchronous:(BOOL)synchronous completion:(KFConnectionManagerJSONCompletionBlock)completion failure:(KFConnectionManagerFailureBlock)failure;
- (void)sendJSONRequest:(NSURLRequest *)request synchronous:(BOOL)synchronous evaluateResponse:(BOOL)evaluateResponse completion:(KFConnectionManagerJSONCompletionBlock)completion failure:(KFConnectionManagerFailureBlock)failure;

#pragma mark -
#pragma mark - image loading

- (void)loadImageAtURL:(NSURL *)url completion:(KFConnectionManagerCompletionBlock)completion failure:(KFConnectionManagerFailureBlock)failure;
- (void)uploadImage:(UIImage *)image atURL:(NSURL *)url completion:(KFConnectionManagerJSONCompletionBlock)completion failure:(KFConnectionManagerFailureBlock)failure;
- (void)uploadImage:(UIImage *)image synchronous:(BOOL)synchronous atURL:(NSURL *)url completion:(KFConnectionManagerJSONCompletionBlock)completion failure:(KFConnectionManagerFailureBlock)failure;


@end
