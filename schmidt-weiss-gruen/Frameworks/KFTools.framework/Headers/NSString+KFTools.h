//
//  NSString+KFTools.h
//  KFTools
//
//  Created by Kevin Flachsmann on 01.03.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

@import Foundation;
@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@interface NSString (KFTools)

- (nullable NSString *)URLEncode;
- (nullable NSString *)URLDecode;
- (nullable NSString *)trimString;


- (CGFloat)fontSizeThatFitsWidthWithSystemFont:(CGFloat)width;

@end

NS_ASSUME_NONNULL_END
