//
//  KFDataManager.h
//  KFTools
//
//  Created by Kevin Flachsmann on 11.05.18.
//  Copyright © 2018 Kevin Flachsmann. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreData;

NS_ASSUME_NONNULL_BEGIN

@interface KFDataManager : NSObject

@property (readonly) NSString *modelName;
@property (readonly) NSString *databaseVersion;
@property (readonly) NSManagedObjectContext *viewContext; //UI operations

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithName:(NSString *)name databaseVersion:(NSString *)version NS_DESIGNATED_INITIALIZER;

- (void)clearDatabase;

- (NSManagedObjectContext *)viewContext;
- (NSManagedObjectContext *)newBackgroundContext;
- (void)performBackgroundTask:(void (^)(NSManagedObjectContext *))block;

@end

NS_ASSUME_NONNULL_END
