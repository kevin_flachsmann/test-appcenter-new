//
//  KFBadgedView.h
//  KFTools
//
//  Created by Kevin Flachsmann on 03.08.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@interface KFBadgeView : UILabel

@end

NS_ASSUME_NONNULL_END
