//
//  KFPopUpViewController.h
//  KFTools
//
//  Created by Kevin Flachsmann on 14.04.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

typedef enum {
    KFPopUpViewControllerTransitionStyleNone,
    KFPopUpViewControllerTransitionStyleBottom,
    KFPopUpViewControllerTransitionStyleTop,
    KFPopUpViewControllerTransitionStyleLeft,
    KFPopUpViewControllerTransitionStyleRight
} KFPopUpViewControllerTransitionStyle;

@interface KFPopUpViewController : UIViewController

@property CGRect contentFrame;
@property CGFloat cornerRadius;
@property BOOL dismissOnTouch;
@property KFPopUpViewControllerTransitionStyle transitionStyle;

- (id)initWithViewController:(UIViewController *)viewController;
- (id)initWithView:(UIView *)view;

- (void)show;
- (void)show:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
