//
//  Colors.h
//  Maschinenring Hannover
//
//  Created by Kevin Flachsmann on 24.08.16.
//  Copyright © 2016 Kevin Flachsmann. All rights reserved.
//

#ifndef Colors_h
#define Colors_h

#define PRIMARY_COLOR_ACCENT [UIColor colorWithRed:52.0/255.0 green:74.0/255.0 blue:94.0/255.0 alpha:1]
#define PRIMARY_COLOR_DARK [UIColor colorWithRed:108.0/255.0 green:143.0/255.0 blue:171.0/255.0 alpha:1]
#define PRIMARY_COLOR [UIColor colorWithRed:73.0/255.0 green:73.0/255.0 blue:73.0/255.0 alpha:1]
#define SELECTED_TABLE_VIEW_CELL_COLOR [UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1]

#endif /* Colors_h */
