//
//  AppDelegate.h
//  winter-pro
//
//  Created by Kevin Flachsmann on 12.10.15.
//  Copyright © 2015 Kevin Flachsmann. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

